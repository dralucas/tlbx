#! /bin/csh -f
#
# ISIS2ENVI
# ---------
# Author: 
#        A. Lucas, Caltech, 2011 
#    
# Contents: 
#        This c-shell script generates envi file with appropriate header from ISIS2 cube
#
# Requierments: 
#        ISIS3 need to be installed and setup on the system (e.g. catlab command is called) 
#
#---------------------------------------------------------------------------------------
clear
echo "ISIS2ENVI"
if ($#argv != 1) then
   clear
   echo " "
   echo " Usage: $0 isiscube.cub" 
   echo " "  
   goto done
endif

set file=$1
set envi_header=$file.hdr

#- CORE KEYWORDS
set samples=`catlab from=$file | grep CORE_ITEMS | awk '{print $3}' | awk -F'[, ]' '{print $1}' | awk -F'[ (]' '{print $2}'`
set lines=`catlab from=$file | grep CORE_ITEMS | awk '{print $4}' | awk -F'[, ]' '{print $1}'`
set bands=`catlab from=$file | grep CORE_ITEMS | awk '{print $5}' | awk -F'[) ]' '{print $1}'`
set type=`catlab from=$file | grep CORE_ITEM_BYTES | awk '{print $3}'`

set size=`du -b $file | awk '{print $1}'`


set rbytes=`catlab from=$file | grep RECORD_BYTES | awk '{print $3}'`
set cqube=`catlab from=$file | grep QUBE | awk '{print $3}'| head -n 1`
set offset=`echo "$rbytes*($cqube-1)" | bc -l `
##set offset=`echo "$size - $type*$samples*$lines" | bc -l` #should have a bette way to figure out the actual offset

#TODO
  #CORE_ITEM_BYTES
  #CORE_ITEM_TYPE
  #CORE_BASE
  #CORE_MULTIPLIER
  
  
#- INSTRUMENT KEYWORDS  
set Instrument=`catlab from=$file | grep INSTRUMENT_NAME | awk '{print $0}' | awk -F'["]' '{print $2}'`
set ProductId=`catlab from=$file | grep PRODUCT_ID | awk '{print $3}' | head -n 1`
set Target=`catlab from=$file | grep TARGET_NAME | awk '{print $3}'`  



  
  
#- MAP PROJECTION KEYWORDS  
set EquatorialRadius=`catlab from=$file | grep A_AXIS_RADIUS | awk '{print $3}'`
set EquatorialRadius=`echo "$EquatorialRadius*1000" | bc -l`

set PolarRadius=`catlab from=$file | grep C_AXIS_RADIUS | awk '{print $3}'`
set PolarRadius=`echo "$PolarRadius*1000" | bc -l`

set reso=`head -n 5000 $file | grep MAP_RESOLUTION  | awk '{print $3}'`

set minLat=`head -n 5000 $file | grep MINIMUM_LATITUDE | awk '{print $3}'`
set maxLat=`head -n 5000 $file | grep MAXIMUM_LATITUDE  | awk '{print $3}'`

set ProjectionName=`catlab from=$file | grep MAP_PROJECTION_TYPE  | awk '{print $3}'`

set PixelResolution=`catlab from=$file | grep MAP_SCALE  | awk '{print $3}'`
set PixelResolution=`echo "$PixelResolution*1000" | bc -l`

set UpperLeftCornerX=`catlab from=$file | grep LINE_PROJECTION_OFFSET | awk '{print $3}'`
set UpperLeftCornerX=`echo "$UpperLeftCornerX * $PixelResolution" | bc -l`
set UpperLeftCornerY=`catlab from=$file | grep SAMPLE_PROJECTION_OFFSET  | awk '{print $3}'`
set UpperLeftCornerY=`echo "- $UpperLeftCornerY * $PixelResolution" | bc -l`

set CenterLatitude=`catlab from=$file | grep CENTER_LATITUDE  | awk '{print $3}'`
set CenterLongitude=`catlab from=$file | grep CENTER_LONGITUDE  | awk '{print $3}'`

set ellips=$Target

set mapinfo="$ProjectionName, 1.0000, 1.0000,$UpperLeftCornerX,$UpperLeftCornerY,$PixelResolution,$PixelResolution,$ellips,"units=Meters""

#TODO
 #IMPLEMENT ALL SUPPORTED PROJECTION
if ($ProjectionName == "ORTHOGRAPHIC") then 
set projnum=14
set projinfo="$projnum, $EquatorialRadius , $CenterLatitude,$CenterLongitude, 0.0, 0.0, $ellips, $ProjectionName, "units=Meters""
endif

if ($ProjectionName == "SINUSOIDAL") then 
set projnum=16
set projinfo="$projnum, $EquatorialRadius , $CenterLongitude, 0.0, 0.0, $ellips, $ProjectionName, "units=Meters""
endif

if ($ProjectionName == "EQUIRECTANGULAR") then 
set projnum=17
set projinfo="$projnum, $EquatorialRadius , $CenterLatitude,$CenterLongitude, 0.0, 0.0, $ellips, $ProjectionName, "units=Meters""
endif

if ($ProjectionName == "MERCATOR") then 
set projnum=20
set projinfo="$projnum, $PolarRadius, $EquatorialRadius , $CenterLatitude,$CenterLongitude, 0.0, 0.0, $ellips, $ProjectionName, "units=Meters""
endif

if ($ProjectionName == "POLAR_STEREOGRAPHIC") then 
set projnum=31
set projinfo="$projnum, $PolarRadius, $EquatorialRadius , $CenterLatitude,$CenterLongitude, 0.0, 0.0, $ellips, $ProjectionName, "units=Meters""
endif

echo " " 
echo "informations: "
echo "------------------ "
echo 'File: '$file
echo "  Samples:" $samples 
echo "  lines : " $lines
if ( $bands != 1) echo "  Bands : " $bands 
echo "  Type : "  $type
echo "  Target:"  $Target
echo "  Instrument:" $Instrument
echo "  Projection: "$ProjectionName
echo " "


#- WRITING INTO ENVI HEADER FILE
echo "ENVI" > $envi_header
echo "description = {"  >> $envi_header
echo " " $ProductId,         >> $envi_header
echo " " $Instrument,      >> $envi_header
echo "  Target Name= " $Target,        >> $envi_header
echo "  File Imported from ISIS-2 into ENVI by "$user"}" >> $envi_header
echo "samples = "$samples             >> $envi_header
echo "lines = "$lines                 >> $envi_header
echo "bands = "$bands                 >> $envi_header
echo "header offset = "$offset        >> $envi_header
echo "file type =  ENVI Standard"     >> $envi_header
echo "data type = " $type             >> $envi_header
echo "interleave = bsq"               >> $envi_header
echo "sensor type = "$Instrument      >> $envi_header
echo "byte order = 0"                 >> $envi_header
echo "map info = {"$mapinfo"}"      >> $envi_header
echo "projection info = {"$projinfo"}" >> $envi_header
echo "default stretch = 0.000000 1.000000 linear" >> $envi_header

done:
  exit 0
