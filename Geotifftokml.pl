#!/usr/bin/perl -s
# // Geotifftokml.pl //
# 
#
# Created by Antoine Lucas on 7/23/10.
# 2010 Caltech.
#
# Requierments: 
#   - POSIX System (UNIX,Linux, BSD, OS X)
#   - ExifTool (url: http://www.sno.phy.queensu.ca/~phil/exiftool/)
#   - Google Earth (release 5+)
########################################################################

use File::Copy;
use Math::Trig;
use Math::Trig ':pi';
use Math::Trig ':radial';
use Math::Trig ':great_circle';
system('clear');

$myprog = "Geotifftokml";

my $usage = "
   Usage: $myprog Geotiff


 Requierments: 
   - POSIX System (UNIX,Linux, BSD, OS X)
   - ExifTool (url: http://www.sno.phy.queensu.ca/~phil/exiftool/)
   - ImageMagick
   - Google Earth (release 5+)


";


#--------------------------------------------------------------------
# Forces a buffer flush after every print, printf, and write on the
# currently selected output handle.  Let's you see output as it's
# happening.
#---------------------------------------------------------------------
$| = 1;


#---------------------------------------------------------------------
# Check the argument list
#---------------------------------------------------------------------
if ($#ARGV ne 0)
{
	print "$usage\n";
	exit 1;
}


$geotiff = $ARGV[0];
$firstdot = index($geotiff,".");
$core_name = substr($geotiff,0,$firstdot);


#---------------------------------------------------------------------
# Get System parameters
#---------------------------------------------------------------------
$usr =`printenv | grep USER=`;
$equal = index($usr,"=");
$user = substr($usr,$equal+1,);
$blank = index($user," ");
$user = substr($user,0,$blank);
$sys = `uname -s`;
$arch= `uname -m`;
$machine = `uname -n`;
$blank = index($machine," ");
$machine = substr($machine,0,$blank);
$date=`date`;
$blank = index($date,"  ");
$date = substr($date,0,$blank);
$pwd=`/bin/pwd`;


#---------------------------------------------------------------------
# Open LOG file
#---------------------------------------------------------------------
if (-e "$core_name.err") {unlink("$core_name.err");}
$log = "$core_name.err";
open (LOG,">$log") or die "\n Cannot open $log\n";


#-----let's check if ImageMagick is established
$exiftooltest=`which convert | grep convert`;
unless ( $exiftooltest)
{
	print "\n*** ERROR *** \n\n ";
	print "ImageMagick is not established\n\n";
	print " $myprog will terminate\n\n";
	print LOG "\n*** ERROR ***\n";
	print LOG "$date\n";
	print LOG "User: $user\n";
	print LOG "Host: $machine\n";
	print LOG "ImageMagick is not established on $machine\n";
	exit 1;
}


#-----let's check if exiftool is established
$exiftooltest=`which exiftool | grep exiftool`;
unless ( $exiftooltest)
{
	print "\n*** ERROR *** \n\n ";
	print "exiftool is not established\n\n";
	print " $myprog will terminate\n\n";
	print LOG "\n*** ERROR ***\n";
	print LOG "$date\n";
	print LOG "User: $user\n";
	print LOG "Host: $machine\n";
	print LOG "exiftool is not established on $machine\n";
	exit 1;
}


#-----let's check if the file exists
if (!(-e $geotiff))
{
	print "\n*** ERROR *** \n\n Input GeoTiff file $geotiff, does not exist.\n\n";
	print " $myprog will terminate\n\n";
	print LOG "\n*** ERROR ***\n";
	print LOG "$date\n";
	print LOG "User: $user\n";
	print LOG "Host: $machine\ns";
	print LOG "Input GeoTiff file $geotiff, does not exist in $pwd\n";
	exit 1;
}


#check if tiff 
$iftiff=`exiftool $geotiff | grep \"TIFF\" `;
unless ($iftiff)
{
	print "\n*** ERROR *** \n\n Input is not Tiff file.\n\n";
	print " $myprog will terminate\n\n";
	print LOG "\n*** ERROR ***\n";
	print LOG "$date\n";
	print LOG "User: $user\n";
	print LOG "Host: $machine\n";
	print LOG "Input is not Tiff file.";
	exit 1;	
}



#check if Geotiff data:
$ifgeotiff=`exiftool $geotiff | grep Geog `;
unless ($ifgeotiff)
{
	print "\n*** ERROR *** \n\n Input does not contain Geotiff tags.\n\n";
	print " $myprog will terminate\n\n";
	print LOG "\n*** ERROR ***\n";
	print LOG "$date\n";
	print LOG "User: $user\n";
	print LOG "Host: $machine\n";
	print LOG "Input does not contain Geotiff tags.";
	exit 1;	
}




#check if SOCET SET data:
$socetset=`exiftool $geotiff | grep \"SOCET-SET\" `;
$issocet = "YES";
unless ($socetset)
{
	$issocet = "NO (Rotated under IDL ?)";
	#print "\n*** ERROR *** \n\n Input is not a SOCET-SET output file.\n\n";
	#print " $myprog will terminate\n\n";
	#print LOG "\n*** ERROR ***\n";
	#print LOG "$date\n";
	#print LOG "User: $user\n";
	#print LOG "Host: $machine\n";
	#print LOG "Input is not a SOCET-SET output file.";
	#exit 1;	
}


#------BEGIN of Body----------
print "- GeoTiff to KML -\n";
print "______________________________________\n";
print " User: $user\n";
print " Host: $machine\n";
print " Arch: $arch";
print " System: $sys";
print " Today is $date\n";
print "______________________________________";
print " \n";
print " \n";
print "Working on $geotiff\n";
print " File Type: TIFF\n";
print " Geo Tags: YES\n";
#print " SOCET-SET tagged: $issocet\n";




#converting into Alpha-PNG of 1024 pixels max.
$png =$core_name . ".png";
$jpg =$core_name . ".jpg";
$jpg0 =$core_name . "-0.jpg";
$res = "1024";

#$cmd = `convert -quiet -limit memory 300mb -resize $res $geotiff $jpg` ;
#print $cmd;
#system ($cmd) == 0 || print LOG "Error during converting to jpg.\n";

if (-e $jpg0) {
	rename ($jpg0,"tmp");
	$cmd = `/bin/rm -f $core_name*.jpg`;
	rename ("tmp",$jpg);
};

#$cmd = `convert -quiet -antialias -resize $res -depth 8  -transparent black $jpg $png` ;
if (-e $png) {unlink($jpg)};

#getting geotags
if (-e "tmp") {unlink("tmp");}



#check if data are correctly projected:
$ifprojected=`exiftool $geotiff | grep \"Model Tie Point\" `;
unless ($ifprojected)
{
		
	$model=`exiftool $geotiff | grep \"Model Transform\" `;
	open (TMP,">tmp");
	print TMP $model;
	$csx=` awk  '{print \$4}' tmp `;
	$ssx=` awk  '{print \$5}' tmp `;
	$long0=` awk  '{print \$7}' tmp `;

	$ssy=` awk  '{print \$8}' tmp `;
	$csy=` awk  '{print \$9}' tmp `;
	$lat0=` awk  '{print \$11}' tmp `;
	unlink("tmp");

	
	$theta =  atan( - $ssx / $csx);#* (180 / pi);
    $sx = $csx / cos($theta);
	$sy =  - $csy / cos($theta);
	$theta = - $theta* (180 / pi);
	
	
	
		
    $w = `exiftool $geotiff | grep \"Image Width\" `;
	$h = `exiftool $geotiff | grep \"Image Height\" `;
	open (TMP,">tmp");
	print TMP $w;
	$w=` awk  '{print \$4}' tmp `;
	unlink("tmp");
	open (TMP,">tmp");
	print TMP $h;
	$h=` awk  '{print \$4}' tmp `;
	unlink("tmp");
	
	
	$blank = index($lat0,"  ");
	$lat0 = substr($lat0,0,$blank);
	$blank = index($long0,"  ");
	$long0 = substr($long0,0,$blank);
	$latf = $lat0 - $h*$sy;
	$longf = $long0 + $w*$sx;
	
	
	print "$sx\n, $sy\n, $theta\n, $lat0\n, $long0\n, $latf\n, $longf\n, $h\n, $w";
	
	
	
	
} else 
{
		
	$pxscale=`exiftool $geotiff | grep \"Pixel Scale\" `;
	$tie=`exiftool $geotiff | grep \"Tie Point\" `;
	$w = `exiftool $geotiff | grep \"Image Width\" `;
	$h = `exiftool $geotiff | grep \"Image Height\" `;
	$planet = `exiftool $geotiff | grep \"Geog Citation\" `;
	open (TMP,">tmp");
	print TMP $pxscale;
	$sx=` awk  '{print \$4}' tmp `;
	$sy=` awk '{print \$5}' tmp `;
	unlink("tmp");
	open (TMP,">tmp");
	print TMP $tie;
	$long0=` awk  '{print \$8}' tmp `;
	$lat0=` awk '{print \$9}' tmp `;
	unlink("tmp");
	$blank = index($lat0,"  ");
	$lat0 = substr($lat0,0,$blank);
	$blank = index($long0,"  ");
	$long0 = substr($long0,0,$blank);
	open (TMP,">tmp");
	print TMP $w;
	$w=` awk  '{print \$4}' tmp `;
	unlink("tmp");
	open (TMP,">tmp");
	print TMP $h;
	$h=` awk  '{print \$4}' tmp `;
	unlink("tmp");
	$latf = $lat0 - $h*$sy;
	$longf = $long0 + $w*$sx;
	$theta = "0"; 


}












#Because Google Earth uses ocentric coordinates system for Mars, let's convert image coordinates
$a = "3396.19";
$b = "3396.19";
$c = "3376.20";

$lat0oc =  atan( ($c/$a)**2 * tan($lat0*(pi/180)) ) * (180 / pi);
$latfoc = atan( ($c/$a)**2 * tan($latf*(pi/180)) ) * (180 / pi);

$latc = ($lat0oc + $latfoc) / 2 ;
$longc = ($longf + $long0) / 2;


#let's check that variable are not empty



#output into KML file
$kml =$core_name . ".kml";
$bck =$core_name . ".kml.bck";

if (-e $kml) {
	print "\n $kml already exists, let's backup it\n";
	rename ($kml,$bck);
}

open (KML,">$kml");

print KML "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<kml xmlns=\"http://www.opengis.net/kml/2.2\" hint=\"target=mars\">
<Document>\n";
#print KML " <Folder>\n<name>Processed DTM Footprints</name>\n";
print KML "    <visibility>1</visibility>\n";
print KML "     <GroundOverlay>\n";
print KML " <name>$core_name Footprint</name>\n";
print KML "   <description>$longc E , $latc N.\n DTM and Orthophoto are located in $machine\:$pwd\n \n Created on $date\n by $user. \n Division of Geological and Planetary Sciences - Caltech.\n All rights reserved.</description>\n";
print KML "      <Icon>\n";
print KML "       <href>$core_name.png</href>\n";
print KML "      </Icon>\n";
print KML "      <LatLonBox>\n";
print KML "       <north>$lat0oc</north>\n";
print KML "       <south>$latfoc</south>\n";
print KML "       <east>$longf</east>\n";
print KML "       <west>$long0</west>\n";
print KML "       <rotation>$theta</rotation>\n";
print KML "      </LatLonBox>\n";
print KML "      </GroundOverlay>\n";
#print KML "     </Folder>\n";
print KML "</Document>\n";
print KML "</kml>";
close (KML);


#------END of Body----------


#---------------------------------------------------------------------
# Close the LOG file.
# If an error was detected, print out the log file
#---------------------------------------------------------------------

close (LOG);

@lines = `cat $log`;
if (scalar(@lines) > 0)
{
	print "\n*** Errors detected in processing ***\n\n";
	print @lines;
	print "\n";
}
else
{
	print "\nProcessing done: ";
	print "Open your new KML file with Google Earth\n\n";
	unlink ($log);
}

exit;
